import uuid
import pytest
from wishlist.service.users import create_admin
from wishlist.serializers.users import UserModel


@pytest.fixture
async def create_another_customer(client):
    response = await client.post(
        "/customers",
        json=dict(name="test2", email="test2@test.com", password="test12345"),
    )
    return response


@pytest.mark.asyncio
async def test_can_create_customer(customer_create):
    response = customer_create
    response.raise_for_status()
    result = response.json()
    assert result["name"] == "test"


@pytest.mark.asyncio
async def test_cannot_create_customer_with_already_existing_email(
    customer_create, client
):
    response = customer_create
    response = await client.post(
        "/customers",
        json=dict(name="test2", email="test@test.com", password="test12345"),
    )
    assert response.status_code == 400


@pytest.mark.asyncio
async def test_can_get_customer(customer_create, client, admin_token):
    customer = customer_create.json()
    response = await client.get(f"/customers/{customer['id']}?auth={admin_token}")
    response.raise_for_status()
    assert response.json()["email"] == "test@test.com"


@pytest.mark.asyncio
async def test_can_list_customers(
    customer_create, create_another_customer, client, admin_token
):
    response = await client.get(f"/customers?auth={admin_token}")
    response.raise_for_status()
    assert len(response.json()) == 2


@pytest.mark.asyncio
async def test_can_delete_customer(
    customer_create, create_another_customer, client, admin_token
):
    customer = create_another_customer.json()
    response = await client.delete(f"/customers/{customer['id']}?auth={admin_token}")
    response.raise_for_status()
    response = await client.get(f"/customers?auth={admin_token}")
    assert len(response.json()) == 1


@pytest.mark.asyncio
async def test_can_create_admin(client, admin_token):
    response = await client.post(
        f"/admins?auth={admin_token}",
        json=dict(name="admin2", email="admin2@test.com", password="test12345"),
    )
    response.raise_for_status()
    assert response.json()["name"] == "admin2"


@pytest.mark.asyncio
async def test_can_get_admin(client, admin_token):
    response = await client.post(
        f"/admins?auth={admin_token}",
        json=dict(name="admin2", email="admin2@test.com", password="test12345"),
    )
    admin_id = response.json()["id"]
    response = await client.get(f"/admins/{admin_id}?auth={admin_token}")
    response.raise_for_status()
    assert response.json()["email"] == "admin2@test.com"


@pytest.mark.asyncio
async def test_can_list_admin(client, admin_token):
    response = await client.post(
        f"/admins?auth={admin_token}",
        json=dict(name="admin2", email="admin2@test.com", password="test12345"),
    )
    response = await client.get(f"/admins?auth={admin_token}")
    assert len(response.json()) == 2


@pytest.mark.asyncio
async def test_can_delete_admin(client, admin_token):
    response = await client.post(
        f"/admins?auth={admin_token}",
        json=dict(name="admin2", email="admin2@test.com", password="test12345"),
    )
    admin_id = response.json()["id"]
    response = await client.delete(f"/admins/{admin_id}?auth={admin_token}")
    response.raise_for_status()
    assert len(response.json()) == 1
