import pytest
from alembic.config import main
from starlette.config import environ
import asyncio
from async_asgi_testclient import TestClient
import uuid
import pytest

environ["TESTING"] = "TRUE"


@pytest.fixture(scope="module")
def event_loop():
    # loop = asyncio.get_event_loop_policy().new_event_loop()
    # asyncio.set_event_loop(loop)
    yield asyncio.get_event_loop()
    # assert loop.is_running()@pytest.fixture


@pytest.fixture
async def client():
    from wishlist.main import db, get_app

    main(["--raiseerr", "upgrade", "head"])

    async with TestClient(get_app()) as client:
        yield client

    main(["--raiseerr", "downgrade", "base"])


@pytest.fixture
async def admin_create():
    from wishlist.service.users import create_admin
    from wishlist.serializers.users import UserCreateModel

    admin = UserCreateModel(name="admin", password="admin1234", email="admin@admin.com")
    user = await create_admin(admin)
    return user


@pytest.fixture
async def admin_token(admin_create):
    return admin_create["token"]


@pytest.fixture
async def customer_create(client):
    response = await client.post(
        "/customers",
        json=dict(name="test", email="test@test.com", password="test12345"),
    )
    return response


@pytest.fixture
async def customer_create_using_db():
    from wishlist.service.users import create_customer
    from wishlist.serializers.users import UserCreateModel

    cust = UserCreateModel(
        name="customer", password="cust1234", email="customer@customer.com"
    )
    user = await create_customer(cust)
    return user


@pytest.fixture
async def customer_token(customer_create_using_db):
    return customer_create_using_db["token"]
