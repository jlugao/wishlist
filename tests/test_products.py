import pytest


@pytest.mark.asyncio
async def test_can_get_user_wishlist(client, customer_token):
    response = await client.get(f"/wishlist?auth={customer_token}")
    response.raise_for_status()


@pytest.mark.asyncio
async def test_can_add_product_to_wishlist(client, customer_token):
    response = await client.post(
        f"/wishlist/products?auth={customer_token}",
        json={"id": "dce33cfd-ccb1-c972-da0e-eddefcd1346b"},
    )
    response.raise_for_status()
    assert (
        response.json()["result"][0]["foreign_id"]
        == "dce33cfd-ccb1-c972-da0e-eddefcd1346b"
    )


@pytest.mark.asyncio
async def test_cannot_add_duplicated_product_to_wishlist(client, customer_token):
    response = await client.post(
        f"/wishlist/products?auth={customer_token}",
        json={"id": "dce33cfd-ccb1-c972-da0e-eddefcd1346b"},
    )
    response = await client.post(
        f"/wishlist/products?auth={customer_token}",
        json={"id": "dce33cfd-ccb1-c972-da0e-eddefcd1346b"},
    )
    assert response.status_code == 400


@pytest.mark.asyncio
async def test_cannot_add_product_that_doesnt_exist_to_wishlist(client, customer_token):
    response = await client.post(
        f"/wishlist/products?auth={customer_token}",
        json={"id": "wrong_id"},
    )
    assert response.status_code == 400


@pytest.mark.asyncio
async def test_can_remove_product_from_wishlist(client, customer_token):
    response = await client.post(
        f"/wishlist/products?auth={customer_token}",
        json={"id": "dce33cfd-ccb1-c972-da0e-eddefcd1346b"},
    )
    product_id = response.json()["result"][0]["id"]
    response = await client.delete(
        f"/wishlist/products/{product_id}?auth={customer_token}"
    )
    assert response.json()["objects"] == 0
