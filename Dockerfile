
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

WORKDIR /wishlist/


RUN apt update && apt install -y netcat
# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry 



COPY ./ /wishlist
RUN poetry install



CMD ["poetry","run","uvicorn", "wishlist.asgi:app", "--host", "0.0.0.0", "--port", "80"]
