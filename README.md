# Wishlist


To run the project the first time:
```
make start_db
make run_migrations
make start_web
```

To stop running the project you can use `make stop`, and to start `make start`


Once running, the project is available in `http://localhost:8000`. A documentation
is available at `http://localhost:8000/docs`


To get started create a customer at `/customer` and get a token at `/token/`,
then every request can be sent authenticated appending `?auth=<token>` where `<token>`
is the value returned by the `/token/` endpoint

Information about the current user is available at `/me`

## Todo
Since time to deliver the project was short, the following was left to be completed
on further development:

- mock external api calls on tests
- unit testing for the service layer
- better endpoint documentation
- script to create the first admin
- remove trailing / from token route
- improved documentation with appropriate docstrings
