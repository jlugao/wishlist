from ..database import db
from wishlist.serializers.users import UserType


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.BigInteger(), primary_key=True)
    name = db.Column(db.Unicode(), default="")
    email = db.Column(db.Unicode(), default="")
    type = db.Column(db.Integer(), default=UserType.CUSTOMER.value)
    token = db.Column(db.Unicode(), default="")
    password = db.Column(db.Unicode(), default="")
