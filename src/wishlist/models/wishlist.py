from ..database import db


class Wishlist(db.Model):
    __tablename__ = "wishlists"

    id = db.Column(db.BigInteger(), primary_key=True)
    user = db.Column(None, db.ForeignKey("users.id"))


class Product(db.Model):
    __tablename__ = "products"

    id = db.Column(db.BigInteger(), primary_key=True)
    foreign_id = db.Column(db.Unicode())
    wishlist = db.Column(None, db.ForeignKey("wishlists.id"))
    title = db.Column(db.Unicode())
    image = db.Column(db.Unicode())
    price = db.Column(db.Float())
    rating = db.Column(db.Float())
