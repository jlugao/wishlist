from fastapi import APIRouter, HTTPException, Depends
from typing import List
from .utils import token, has_admin_authorization
from wishlist.service.users import get_user_from_token
from wishlist.service.wishlist import (
    add_product,
    get_or_create_wishlist,
    get_product_list_from_user,
    remove_product,
)
from wishlist.serializers.wishlist import ProductModel, ProductInputModel

router = APIRouter()


@router.get("/wishlist")
async def get_wishlist_from_user(auth: str = Depends(token), page: int = 0):
    user = await get_user_from_token(auth)
    return await get_product_list_from_user(user.id, page=page)


@router.post("/wishlist/products")
async def add_product_to_wishlist(
    product: ProductInputModel, auth: str = Depends(token), page: int = 0
):
    user = await get_user_from_token(auth)
    return await add_product(user.id, product.id)


@router.delete("/wishlist/products/{product_id}")
async def remove_product_from_wishlist(product_id: int, auth: str = Depends(token)):
    user = await get_user_from_token(auth)
    return await remove_product(user.id, product_id)


def init_app(app):
    app.include_router(router, tags=["Wishlist"])
