from wishlist.service.users import validate_token, token_belongs_to_admin
from fastapi import HTTPException
from typing import Union


async def token(auth: str) -> Union[str, None]:
    if await validate_token(auth):
        return auth
    raise HTTPException(403, "Unauthorized Access")


async def has_admin_authorization(auth: str) -> Union[str, None]:
    if await token_belongs_to_admin(auth):
        return auth
    raise HTTPException(403, "Unauthorized Access")
