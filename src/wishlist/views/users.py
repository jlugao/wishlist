from fastapi import APIRouter, HTTPException, Depends
from wishlist.serializers.users import UserModel, UserModelOutput, UserLoginModel, UserCreateModel
from wishlist.service.users import (
    fetch_customer,
    create_customer,
    remove_customer,
    list_all_customers,
    fetch_admin,
    create_admin,
    remove_admin,
    list_all_admins,
    get_token,
    get_user_from_token
)
from typing import List
from .utils import token, has_admin_authorization
customer_router = APIRouter()
admin_router = APIRouter()
token_router = APIRouter()


@customer_router.get("/customers", response_model=List[UserModelOutput])
async def list_customers(auth: str = Depends(has_admin_authorization)):
    return await list_all_customers()


@customer_router.get("/customers/{uid}", response_model=UserModelOutput)
async def get_customer(uid: int, auth: str = Depends(has_admin_authorization)):
    return await fetch_customer(uid)


@customer_router.post("/customers", response_model=UserModelOutput)
async def add_customer(user: UserCreateModel):
    return await create_customer(user)


@customer_router.delete("/customers/{uid}")
async def delete_customer(uid: int):
    return await remove_customer(uid)


@admin_router.get("/admins", response_model=List[UserModelOutput])
async def list_admins(auth: str = Depends(has_admin_authorization)):
    return await list_all_admins()


@admin_router.get("/admins/{uid}", response_model=UserModelOutput)
async def get_admin(uid: int, auth: str = Depends(has_admin_authorization)):
    return await fetch_admin(uid)


@admin_router.post("/admins", response_model=UserModelOutput)
async def add_admin(user: UserModel, auth: str = Depends(has_admin_authorization)):
    return await create_admin(user)


@admin_router.delete("/admins/{uid}")
async def delete_admin(uid: int, auth: str = Depends(has_admin_authorization)):
    return await remove_admin(uid)


@token_router.post("/token/")
async def login(user: UserLoginModel):
    try:
        return await get_token(user)
    except ValueError:
        HTTPException(status_code=400, detail="Invalid password or Username")


@token_router.get("/me", response_model=UserModelOutput)
async def get_me(auth: str = Depends(token)):
    user = await get_user_from_token(auth)
    return user.to_dict()


def init_app(app):
    app.include_router(customer_router, tags=["customer"])
    app.include_router(admin_router, tags=["admin"])
    app.include_router(token_router, tags=["token"])
