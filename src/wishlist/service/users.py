from wishlist.models.users import User
from wishlist.serializers.users import UserType, UserModel, UserLoginModel
import secrets
import hashlib
from fastapi import HTTPException


async def get_user_from_token(token: str):
    user_from_db = await User.query.where(User.token == token).gino.first()
    print(user_from_db)
    return user_from_db


async def token_belongs_to_admin(token: str):
    user = await get_user_from_token(token)
    return user is not None and user.type == UserType.ADMIN.value


async def verify_if_user_exists(email: str):
    user_from_db = await User.query.where(User.email == email).gino.first()
    return user_from_db is not None


async def create_customer(user: UserModel):
    token = secrets.token_hex(20)
    if await verify_if_user_exists(user.email):
        raise HTTPException(400, "User with this email already exists")
    hash = hashlib.sha256()
    hash.update(user.password.encode("utf-8"))
    user_object = await User.create(
        name=user.name,
        email=user.email,
        type=UserType.CUSTOMER.value,
        token=token,
        password=hash.hexdigest(),
    )
    return user_object.to_dict()


async def fetch_customer(uid: int):
    user = await User.get_or_404(uid)
    return user.to_dict()


async def remove_customer(uid: int):
    user = await User.get_or_404(uid)
    await user.delete()
    return dict(id=uid)


async def list_all_customers():
    users = await User.query.where(User.type == UserType.CUSTOMER.value).gino.all()
    return [user.to_dict() for user in users]


async def create_admin(user: UserModel) -> dict:
    user_object = await User.create(
        name=user.name, email=user.email, type=UserType.ADMIN.value
    )
    return user_object.to_dict()


async def fetch_admin(uid: int):
    user = await User.get_or_404(uid)
    return user.to_dict()


async def remove_admin(uid: int):
    user = await User.get_or_404(uid)
    await user.delete()
    return dict(id=uid)


async def list_all_admins():
    users = await User.query.where(User.type == UserType.ADMIN.value).gino.all()
    return [user.to_dict() for user in users]


async def validate_token(token: str):
    user_from_db = await User.query.where(User.token == token).gino.first()
    return user_from_db is not None


async def get_token(user: UserLoginModel) -> str:
    user_from_db = await User.query.where(User.email == user.email).gino.first()
    if user_from_db is None:
        raise HTTPException(400, "Invalid data")
    hash = hashlib.sha256()
    hash.update(user.password.encode("utf-8"))
    if hash.hexdigest() == user_from_db.password:
        return user_from_db.token
    else:
        raise ValueError("Invalid Password")
