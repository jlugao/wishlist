from wishlist.models.wishlist import Wishlist, Product
from wishlist.database import db
from wishlist.config import PAGINATION_LIMIT
from fastapi import HTTPException
import httpx
from math import ceil
from wishlist.serializers.wishlist import ProductModel


async def get_product_list_from_wishlist(wishlist_id: int, page: int = 0) -> dict:
    query = (
        db.select([db.func.count()])
        .select_from(Product)
        .where(Product.wishlist == wishlist_id)
    )
    product_count = await query.gino.scalar()
    pages = ceil(product_count / PAGINATION_LIMIT)
    products = (
        await Product.query.where(Product.wishlist == wishlist_id)
        .order_by(Product.id.desc())
        .limit(PAGINATION_LIMIT)
        .offset(PAGINATION_LIMIT * page)
        .gino.all()
    )
    products = [ProductModel(**product.to_dict()).dict() for product in products]
    result = {
        "objects": product_count,
        "pages": pages,
        "current": page,
        "result": products,
    }
    return result


async def get_product_list_from_user(user_id: int, page: int = 0) -> dict:
    wishlist_id = await get_or_create_wishlist(user_id)
    return await get_product_list_from_wishlist(wishlist_id, page)


async def get_or_create_wishlist(user_id: int) -> dict:
    wishlist = await Wishlist.query.where(Wishlist.user == user_id).gino.one_or_none()
    if wishlist is not None:
        return wishlist.id
    wishlist = await Wishlist.create(user=user_id)
    return wishlist.id


async def get_product_info(product_id: str) -> dict:

    async with httpx.AsyncClient() as client:
        response = await client.get(
            f"http://challenge-api.luizalabs.com/api/product/{product_id}/"
        )
        if response.status_code != 200:
            raise HTTPException(400, "No such product")
        return response.json()


async def product_exists_in_wishlist(product_id: str, wishlist_id: int) -> bool:
    product = (
        await Product.query.where(Product.wishlist == wishlist_id)
        .where(Product.foreign_id == product_id)
        .gino.one_or_none()
    )
    print(product)
    return product is not None


async def add_product(user_id: int, product_id: str) -> dict:
    wishlist_id = await get_or_create_wishlist(user_id)
    if await product_exists_in_wishlist(product_id, wishlist_id):
        raise HTTPException(400, "Product already exists in wishlist")
    product_info = await get_product_info(product_id)
    product = await Product.create(
        foreign_id=product_info["id"],
        wishlist=wishlist_id,
        title=product_info.get("title", ""),
        image=product_info.get("image", ""),
        price=product_info.get("price", None),
        rating=product_info.get("reviewScore", None),
    )
    return await get_product_list_from_wishlist(wishlist_id)


async def remove_product(user_id: int, product_id: int) -> dict:
    wishlist_id = await get_or_create_wishlist(user_id)
    await Product.delete.where(Product.id == product_id).gino.status()
    return await get_product_list_from_wishlist(wishlist_id)
