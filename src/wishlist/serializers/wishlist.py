from pydantic import BaseModel
from typing import Optional


class ProductInputModel(BaseModel):
    id: str


class ProductModel(BaseModel):
    id: int
    title: Optional[int]
    foreign_id: str
    title: Optional[str]
    price: Optional[float]
    rating: Optional[float]
