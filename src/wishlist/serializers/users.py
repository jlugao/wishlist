from pydantic import BaseModel, EmailStr
from enum import Enum

class UserType(Enum):
    ADMIN = 1
    CUSTOMER = 2

class UserModel(BaseModel):
    name: str
    email: EmailStr

class UserCreateModel(BaseModel):
    name: str
    email: EmailStr
    password: str

class UserModelOutput(UserModel):
    id: int

class UserLoginModel(BaseModel):
    email: EmailStr
    password: str
