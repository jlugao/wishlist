from fastapi import FastAPI
from .database import db
import logging
from importlib import import_module, reload
from .config import INSTALLED_APPS


def get_app():
    app = FastAPI(title="Wishlist")
    db.init_app(app)
    load_modules(app)
    return app


logger = logging.getLogger(__name__)


def load_modules(application=None):
    for app in INSTALLED_APPS:
        app = f"wishlist.views.{app}"
        app = import_module(app)
        logger.info("Loading module: %s", app)
        mod = reload(app)
        if application:
            init_app = getattr(mod, "init_app", None)
            if init_app:
                init_app(application)
