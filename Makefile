start_db:
	docker-compose up -d db

start_web:
	docker-compose up -d web

run_migrations:
	docker-compose run --rm web poetry run alembic upgrade head

start:
	@$(MAKE) start_db
	sleep 3
	@$(MAKE) start_web

stop:
	docker-compose stop
