"""adds wishlist fields

Revision ID: aec91eec4978
Revises: 0a5a63d54898
Create Date: 2020-11-04 10:39:55.925617

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aec91eec4978'
down_revision = '0a5a63d54898'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('wishlists',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('user', sa.BigInteger(), nullable=True),
    sa.ForeignKeyConstraint(['user'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('products',
    sa.Column('id', sa.BigInteger(), nullable=False),
    sa.Column('foreign_id', sa.Unicode(), nullable=True),
    sa.Column('wishlist', sa.BigInteger(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('image', sa.Unicode(), nullable=True),
    sa.Column('price', sa.Float(), nullable=True),
    sa.Column('rating', sa.Float(), nullable=True),
    sa.ForeignKeyConstraint(['wishlist'], ['wishlists.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('products')
    op.drop_table('wishlists')
    # ### end Alembic commands ###
